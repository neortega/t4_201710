package view;

import java.util.Scanner;

import model.data_structures.ILista;
import model.vo.VOAgnoPelicula;
import model.vo.VOPelicula;
import controller.Controller;

public class VistaManejadorPeliculas {
	
	public static void main(String[] args) {
		
		
		Scanner sc=new Scanner(System.in);
		boolean fin=false;
		while(!fin){
			printMenu();
			
			int option = sc.nextInt();
			
			switch(option){
				case 1:
					Controller.cargarPeliculas();
					break;
				case 2:
					System.out.println("Ingrese subcadena de b�squeda:");
					String busqueda=sc.next();
					ILista<VOPelicula> lista=Controller.darListaPeliculas(busqueda);
					System.out.println("Se encontraron "+lista.darNumeroElementos()+" elementos");
					for (VOPelicula voPelicula : lista) {
						System.out.println(voPelicula.getTitulo());
					}
					break;
				case 3:
					System.out.println("Ingrese el tama�o que tendr� el arreglo:");
					int tama�o = sc.nextInt();
				try {
					VOPelicula[] arreglo = Controller.darMuestraArreglo(tama�o);
					System.out.println("Se cre� el siguiente arreglo de pel�culas (pueden haber pel�culas repetidos):");
					for(VOPelicula vop : arreglo)
					{
						System.out.println(vop.getTitulo());
					}
					Controller.ordenarArreglo(arreglo, tama�o);
					
				} catch (Exception e) {
					
					System.out.println("No se pudo crear el arreglo");
				}
					break;
				case 4:
					System.out.println("Ingrese el tama�o que desea que tenga la lista de pel�culas aleatoria");
					int n = sc.nextInt();
					ILista<VOPelicula> list = Controller.darMuestraLista(n);
					System.out.println("Se cre� la siguiente lista con las siguientes pel�culas: ");
					for(VOPelicula vop: list)
					{
						System.out.println(vop.getTitulo());
					}
					break;
				case 5:	
					fin=true;
					break;
			}
			
			
		}
	}

	private static void printMenu() {
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("------------------- Taller 2 ----------------------");
		System.out.println("1. Cree una nueva colecci�n de pel�culas (data/movies.csv)");
		System.out.println("2. Buscar pel�culas por subcadena");
		System.out.println("3. Dar muestra de un arreglo aleatorio por un tama�o establecido (se ordenar� autom�ticamente)");
		System.out.println("4. Dar muestra de una lista aleatoria por un tama�o establecido");
		System.out.println("5. Salir");
		System.out.println("Elija el n�mero que corresponde a la tarea que desea realizar");
		System.out.println("---------------------------------------------------");
		
	}

}
