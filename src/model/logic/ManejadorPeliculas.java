package model.logic;

import java.io.BufferedReader;
import java.io.FileReader;

import model.data_structures.ILista;
import model.data_structures.Mergesort;
import model.vo.VOAgnoPelicula;
import model.vo.VOPelicula;
import api.IManejadorPeliculas;
import model.data_structures.ListaDobleEncadenada;

public class ManejadorPeliculas implements IManejadorPeliculas  {

	/**
	 * Constante con el separador del archivo
	 */
	public final static String SEPARADOR =",";
	/**
	 * Representa la cabeza de mi lista de peliculas VOPeliculas
	 */
	private ILista<VOPelicula> misPeliculas;
	/**
	 * 
	 */
	private Mergesort<VOPelicula> comparador;
	/**
	 * 
	 */
	private ILista<VOPelicula> muestra;
	/**
	 * Carga la informacion del archivo csv
	 * @param archivoPeliculas: contiene la direccion donde se encuentra 
	 * @throws Exception si el formato ingresado no es el esperado
	 */
	public void cargarArchivoPeliculas(String archivoPeliculas) throws Exception {

		try {
			BufferedReader read = new BufferedReader(new FileReader(archivoPeliculas));
			String line = read.readLine();
			while(line!=null)
			{
				String[] partes = line.split(SEPARADOR);
				if(partes.length>3)
				{
					for (int i = 1; i < partes.length-1; i++) {
						partes[1] += partes[i];
					}
				}
				VOPelicula nuevaPelicula= new VOPelicula();
				int agno = Integer.parseInt(partes[1].substring(partes[1].length()-5, partes[1].length()-2));
				String titulo = partes[1].substring(0,partes[1].length()-8);
				nuevaPelicula.setAgnoPublicacion(agno);
				nuevaPelicula.setTitulo(titulo.replace('"', ' '));
				ILista<String> generos = new ListaDobleEncadenada<String>();
				String[] gen = partes[3].split("|");
				for (int i = 0; i < gen.length; i++) {
					generos.agregarElementoFinal(gen[i]);
				}
				nuevaPelicula.setGenerosAsociados(generos);
				misPeliculas.agregarElementoFinal(nuevaPelicula);
			}
			read.close();
		} catch (Exception e) {
			throw new Exception("error al cargar la informacion del archivo ingresado");
		}
	}
	/**
	 * Busca una pelicula basado en una linea de caracteres 
	 * @param busqueda: cadena de caracteres que posee la pelicula que se desea buscar
	 */
	public ILista<VOPelicula> darListaPeliculas(String busqueda) {
		ILista<VOPelicula> referenicaPeli = null;;
		while(misPeliculas.avanzarSiguientePosicion()!=false)
		{			
			VOPelicula peli = misPeliculas.darElementoPosicionActual();
			if(peli.getTitulo().contains(busqueda))
			{
				referenicaPeli = misPeliculas;

			}
			misPeliculas.avanzarSiguientePosicion();
		}
		return referenicaPeli;
	}

	@Override
	public VOPelicula[] darMuestraArreglo(int n) throws Exception
	{
		VOPelicula muestra[] = new VOPelicula[n];
		if(n<misPeliculas.darNumeroElementos())
		{
		int random = (int)(Math.random()*n) ;
		for (int i = 0; i < n; i++) {
			muestra[i] = misPeliculas.darElemento(random);
		}
		}
		else
		{
			throw new Exception();
		}
		return muestra;
	}
	
	@Override
	public void ordenarArreglo (VOPelicula[] a, int n) throws Exception
	{
		Mergesort.sort(darMuestraArreglo(n));
	}
	
	@Override
	public ILista<VOPelicula> darMuestraLista (int n)
	{
		int random = (int)(Math.random()*n) ;
		for (int i = 0; i < n; i++) {
			 muestra.agregarElementoFinal(misPeliculas.darElemento(random));
		}
		return muestra;
	}
}

