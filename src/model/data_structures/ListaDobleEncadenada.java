package model.data_structures;

import java.util.Iterator;
import model.data_structures.NodoDoble;
public class ListaDobleEncadenada<T> implements ILista<T> {

	/**
	 * Atributo que representa la cantidad de elementos en la lista.
	 */
	private int size;
	
	/**
	 * Atributo que representa el elemento inicial de la lista
	 */
	private NodoDoble<T> actual;
	
	/**
	 * Constructor del iterator de la lista
	 */
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return new Iterator<T>()
				{
			private NodoDoble<T> pos = null;

			@Override
			public boolean hasNext() {
				// TODO Auto-generated method stub
				if(size == 0)
					return false;
				if(pos == null)
					return false;
				if(pos.darSiguiente() == null)
					return false;
				else
					return true;
			}

			@Override
			public T next() {
				// TODO Auto-generated method stub
				if(pos == null)
					pos = actual;
				else
					pos = pos.darSiguiente();
				return pos.darObject();
			}
			
				};
	}

	/**
	 * Agrega un elemento al final de la lista
	 * @param elem elemento que se desea agregar != null
	 */
	public void agregarElementoFinal(T elem) {

		if(actual==null)
		{
			actual = new NodoDoble<T>(elem);
		}
		else
		{
			while(actual !=null)
			{
				actual = actual.darSiguiente();
			}				
			NodoDoble<T> cambio = actual;
			actual.modificarSiguiente(new NodoDoble<T>(elem));
			actual.darSiguiente().modificarAnterior(cambio);
		}
		// TODO Auto-generated method stub	
	}

	/**
	 * Retorna el elemento de la posicion dada por parametro de la lista
	 * @param pos posicion donde se encuentra el elemento que se desea retornar
	 * @return elemento de la posicion ingresada
	 */
	public T darElemento(int pos) {

		int contador=0;
		T element = null;

		while (actual!=null) {
			actual = actual.darSiguiente();
			contador++;
			if (contador == pos)
			{
				element = (T) actual.darObject();
				break;
			}
		}
		// TODO Auto-generated method stub
		return element;
	}


	/**
	 * Retorna el tama�o de la lista
	 */
	public int darNumeroElementos() {
		int contador =0;
		while(actual !=null)
		{
			actual = actual.darSiguiente();
			contador++;
		}
		// TODO Auto-generated method stub
		return contador;
	}

	/**
	 * Retorna el elemento del nodo actual
	 * @return T: elemento de la posicion 
	 */
	public T darElementoPosicionActual() {
		return (T) actual.darObject();
	}

	/**
	 * Avanza una posicion teniendo como referencia el nodo actual
	 * @return sePudo: true si se pudo realizar la accion; false si no se logro
	 */
	public boolean avanzarSiguientePosicion() {
		boolean sePudo = false;
		if(actual.darSiguiente() == null)
		{

		}
		else 
		{
			actual = actual.darSiguiente();
			sePudo = true;
		}
		return sePudo;

	}

	/**
	 * Retrocede una posicion en la lista teniendo como referencia el nodo actual
	 * @return sePudo: true si se logro realizar la operacion; false si no
	 */
	public boolean retrocederPosicionAnterior() {
		boolean sePudo = false;
		if(actual.darAnterior() == null)
		{

		}
		else 
		{
			actual = actual.darAnterior();
			sePudo = true;
		}
		return sePudo;
	}

}
