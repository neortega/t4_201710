package model.data_structures;

import java.util.Iterator;

import model.data_structures.NodoSencillo;

public class ListaEncadenada<T> implements ILista<T> {

	/**
	 * Atributo que representa el elemento inicial de la lista
	 */
	private NodoSencillo<T> actual;
	
	/**
	 * Atributo que representa la cantidad de elementos que tiene la lista.
	 */
	private int size;
	/**
	 * Constructor del iterator de la lista
	 */

	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return new Iterator<T>()
				{

			private NodoSencillo<T> pos = null;
			@Override
			public boolean hasNext() {
				// TODO Auto-generated method stub
				if(size==0)
				{
					return false;
				}
				if(pos ==null)
				{
					return true;
				}
				if(pos.darSiguiente()==null){
					return false;
				}
				else
				{
					return true;
				}
			}

			@Override
			public T next() {
				if(pos==null)
				{
					pos = actual;
				}
				else{
					pos = pos.darSiguiente();
				}
				return pos.darObjeto();
			}			
				};
	}

	/**
	 * Agrega un elemento al final de la lista
	 * @param elem elemento que se desea agregar != null
	 */
	public void agregarElementoFinal(T elem) {
		if(actual==null)
		{
			actual = new NodoSencillo<T>(elem);
		}
		else
		{
			while(actual !=null)
			{
				actual = actual.darSiguiente();
			}				
			actual.modificarSiguiente(elem);
		}
		// TODO Auto-generated method stub
		if (actual == null)
		{
			actual = new NodoSencillo<T>(elem);
		}
		while(actual != null)
		{
			if(actual.darSiguiente() == null)
			{
				actual.modificarSiguiente(elem);
			}
			else
				actual = actual.darSiguiente();
		}
	}

	/**
	 * Retorna el elemento de la posicion dada por parametro de la lista
	 * @param pos posicion donde se encuentra el elemento que se desea retornar
	 * @return elemento de la posicion ingresada
	 */
	public T darElemento(int pos) {
		// TODO Auto-generated method stub
		T elem = null;
		int cont = 0;
		if(cont == pos)
			elem = actual.darObjeto();
		
		while(actual.darSiguiente() != null)
		{
			actual = actual.darSiguiente();
			cont ++;
			if(cont == pos)
			{
				elem = actual.darObjeto();
			}
		}
		return elem;
	}


	/**
	 * Retorna el tama�o de la lista
	 */
	public int darNumeroElementos() {
		// TODO Auto-generated method stub
		int cont = 0;
		
		if(actual.darSiguiente() == null)
			cont = 1;
		
		while(actual.darSiguiente() != null)
		{
			actual = actual.darSiguiente();
			cont ++;
		}
		size = cont;
		return size;
	}

	/**
	 * Retorna el elemento del nodo actual
	 * @return T: elemento de la posicion 
	 */
	public T darElementoPosicionActual() {
		// TODO Auto-generated method stub
		T elem = null;
		
		if(actual != null)
			elem = actual.darObjeto();

		return elem;
	}

	/**
	 * Avanza una posicion teniendo como referencia el nodo actual
	 * @return sePudo: true si se pudo realizar la accion; false si no se logro
	 */
	public boolean avanzarSiguientePosicion() {
		// TODO Auto-generated method stub
		if(actual.darSiguiente() != null)
		{
			actual = actual.darSiguiente();
			return true;
		}
		
		return false;
	}

	/**
	 * Retrocede una posicion en la lista teniendo como referencia el nodo actual
	 * @return sePudo: true si se logro realizar la operacion; false si no
	 */
	public boolean retrocederPosicionAnterior() {
		// TODO Auto-generated method stub
		while(actual.darSiguiente() != null)
		{
			NodoSencillo<T> siguiente = actual.darSiguiente();
			if(siguiente.darSiguiente() == null){
				actual = siguiente;
				return true;
			}
			else
				actual = actual.darSiguiente();
		}
		
		return false;
	}

}
