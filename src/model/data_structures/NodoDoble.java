package model.data_structures;

public class NodoDoble<T> {

	private NodoDoble<T> anterior;
	
	private NodoDoble<T> siguiente;
	
	private T objeto;
	
	public NodoDoble (T pObjeto)
	{
		objeto=pObjeto;
		siguiente=null;
		anterior=null;
	}
	
	public NodoDoble<T> darAnterior()
	{
		return anterior;
	}
	public NodoDoble<T> darSiguiente()
	{
		return siguiente;
	}
	public T darObject()
	{
		return objeto;
	}
	
	public void modificarSiguiente(NodoDoble<T> pSiguiente)
	{
		siguiente = pSiguiente;
				
	}
	public void modificarAnterior( NodoDoble<T> pAnterior)
	{
		anterior = pAnterior;
	}
}
