package model.data_structures;

public class NodoSencillo <T> {
	
	private T objeto;
	
	private NodoSencillo<T> siguiente;
	
	public NodoSencillo(T pObjeto)
	{
		objeto = pObjeto;
	}
	
	public T darObjeto()
	{
		return objeto;
	}
	
	public NodoSencillo<T> darSiguiente()
	{
		return siguiente;
	}
	
	public void modificarSiguiente(T pNuevoSiguiente)
	{
		siguiente =  new NodoSencillo<T>(pNuevoSiguiente);
	}
}
