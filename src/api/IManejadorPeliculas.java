package api;

import model.data_structures.ILista;
import model.vo.VOAgnoPelicula;
import model.vo.VOPelicula;

/**
 * Interface de servicios que presta el manejador de pel�culas <br>
 * El a�o de referencia inicial para los servicios debe ser el primero disponible en la aplicaci�n (1950)
 */
public interface IManejadorPeliculas {
	
	/**
	 * Inicializa el manejador de pel�culas
	 * @param archivoPeliculas archivo en formato csv con pel�culas
	 * @throws Exception 
	 */
	public void cargarArchivoPeliculas(String archivoPeliculas) throws Exception;
	
	/**
	 * Devuelve una lista de pel�culas cuyo t�tulo incluya la subcadena busqueda
	 * @param busqueda subcadena a buscar en los t�tulos
	 * @return Lista con pel�culas cuyo t�tulo incluye la subcadena busqueda
	 */
	public ILista<VOPelicula> darListaPeliculas(String busqueda);
	
	/**
	 * Devuelve un arreglo de pel�culas con el tama�o definido por par�metro
	 * @param n - Tama�o del arreglo de pel�culas
	 * @return arreglo de pel�culas con el tama�o predefinido 
	 * @throws Exception - Lanza excepci�n si no puede generar el arreglo
	 */
	public VOPelicula[] darMuestraArreglo(int n) throws Exception;
	
	/**
	 * Ordena un arreglo de pel�culas pasado por par�metro
	 * @param a - Arreglo de pel�culas a ordenar
	 * @param n - Tama�o que tendr� el arreglo
	 * @throws Exception - Lanza excepci�n si no puede ordenar el arreglo
	 */
	public void ordenarArreglo(VOPelicula[] a, int n) throws Exception;
	
	/**
	 * Genera una lista de pel�culas con el tama�o definido por par�metro
	 * @param n - Tama�o a desear de la lista
	 * @return Lista con el tama�o predefinido
	 */
	public ILista<VOPelicula> darMuestraLista(int n);
}
