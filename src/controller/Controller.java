package controller;

import model.data_structures.ILista;
import model.logic.ManejadorPeliculas;
import model.vo.VOAgnoPelicula;
import model.vo.VOPelicula;
import api.IManejadorPeliculas;

public class Controller {

	
	private static IManejadorPeliculas manejador= new ManejadorPeliculas();

	public static void cargarPeliculas() {
		
		try{
		manejador.cargarArchivoPeliculas("data/movies.csv");
		}
		catch(Exception e)
		{
			//
		}
		
	}

	public static ILista<VOPelicula> darListaPeliculas(String busqueda) {
		return manejador.darListaPeliculas(busqueda);
	}
	
	public static VOPelicula[] darMuestraArreglo(int n) throws Exception
	{
		return manejador.darMuestraArreglo(n);
	}
	
	public static void ordenarArreglo(VOPelicula[]a, int n) throws Exception
	{
		manejador.ordenarArreglo(a, n);
	}
	
	public static ILista<VOPelicula> darMuestraLista(int n)
	{
		return manejador.darMuestraLista(n);
	}
}
